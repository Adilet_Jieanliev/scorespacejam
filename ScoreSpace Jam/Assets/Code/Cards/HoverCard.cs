using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HoverCard : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler, IPointerUpHandler, IBeginDragHandler,IDragHandler, IEndDragHandler
{
    [Header("CardsPosition")]
    public Vector2 pos = new Vector2 (0, 0);
    public Vector2 posOffset = new Vector2 (0, 0);
    public Vector3 scale = new Vector3 (1, 1, 1);

    [HideInInspector] public int listIndex = 0;
    #region interfaces
    public void OnPointerEnter(PointerEventData eventData) {
        HandController.ActiveCardIndex = listIndex;
        transform.SetAsLastSibling();
    }

    public void OnPointerExit(PointerEventData eventData) {
        HandController.ActiveCardIndex = -404;
        transform.SetSiblingIndex(listIndex);
        posOffset = Vector3.zero;
    }


    public void OnPointerDown(PointerEventData eventData) {

    }
    public void OnPointerUp(PointerEventData eventData) {
    }

    public void OnBeginDrag(PointerEventData eventData) {
    }

    public void OnDrag(PointerEventData eventData) {
        pos = eventData.position;
    }

    public void OnEndDrag(PointerEventData eventData) {
    }
    #endregion
}
