using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor.ShaderGraph.Internal;
using UnityEngine;

public class HandController : MonoBehaviour
{
    public List<HoverCard> cards = new List<HoverCard>();

    [Header("Settings")]
    [SerializeField] private float offset;
    [SerializeField] private float height;
    [SerializeField] private Vector2 scale;
    [SerializeField] private float smoothSpeed;
    [Header("Hover value")]
    [SerializeField] float hoverHeight;
    [SerializeField] Vector2 hoverScale;
    [SerializeField] private float hoverSpeed;
    [SerializeField] private float hoverScaleSpeed;

    private Vector2 myPos;
    private Vector2 myLocalPos;

    public static int ActiveCardIndex = -404;
    private void Awake() {
        myPos = transform.position ;
        myLocalPos = transform.localPosition ;
    }

    private void Update() {
        MoveCardToPos();
    }
    private void MoveCardToPos() {
        Vector2 startPos;
        startPos.x = (myPos.x - ((offset * ((float)cards.Count / 2)) + offset / 2));
        startPos.y = myPos.y + height;

        for (int i = 0; i < cards.Count; i++) {
            startPos.x += offset;

            float moveSpeed = smoothSpeed;
            float scaleSpeed = smoothSpeed;
            if (i == ActiveCardIndex) {
                moveSpeed = hoverSpeed;
                scaleSpeed = hoverScaleSpeed;

                cards[i].posOffset.y = hoverHeight;
                cards[i].scale = hoverScale;
            } else {
                cards[i].scale = scale;
                cards[i].pos = startPos;
            }

            cards[i].transform.position = Vector2.Lerp(cards[i].transform.position, startPos + cards[i].posOffset, moveSpeed * Time.deltaTime);
            cards[i].transform.localScale = Vector3.Lerp(cards[i].transform.localScale, cards[i].scale, scaleSpeed * Time.deltaTime);
            cards[i].listIndex = i;
        }
    }
}
