using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "Game Data", fileName = "Create new Wapon")]
public class WeaponData : ScriptableObject
{
    public string weaponID;
    [Header("Settings")]
    [Tooltip("����� �� ���������� ������ ��� ����� �������� �� ������")]
    public bool hold;
    public int damage;
    public int mass;
    public float bulletForce;
    public �artridgeType �artridgeType;

    [Header("Amount")]
    public int rowAmount;
    public float rowDeltaAngle;
    public int magazineMax;

    [Header("Delays")]
    public float reloadTime;
    public float firingRate;
    public float bulletLifeTime;

    [Header("Links")]
    public GameObject bullet;

    [Header("Art")]
    public Sprite weaponSprite;
    public AudioClip sound;
}
public enum �artridgeType {
    Bullet,
    Battery,
}
