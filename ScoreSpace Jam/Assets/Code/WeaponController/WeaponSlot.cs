using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using UnityEngine;

public class WeaponSlot : MonoBehaviour
{
    public List<Weapon> createdWeapon= new List<Weapon>();
    public Transform weaponParent;
    public GameObject weaponObj;

    private void Awake() {
        
    }

    public void CreateWeapon(List<WeaponData> data) {
        for (int i = 0; i < data.Count; i++) {
            Weapon a = Instantiate<GameObject>(weaponObj, weaponParent).GetComponent<FirearmsWeapon>();
            a.Data = data[i];

            createdWeapon.Add(a);


            a.gameObject.SetActive(false);
        }
        if (data.Count > 0)
            createdWeapon[0].gameObject.SetActive(true);
        else  Debug.LogError("������ ��� �������� ������ !!!"); 
        
    }
}
