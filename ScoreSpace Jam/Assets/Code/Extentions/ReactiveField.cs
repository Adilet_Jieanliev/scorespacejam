﻿
using UnityEngine;
using UnityEngine.Events;

//Своя реактивная переменная
public class ReactiveField<T> {
    [SerializeField] private T Value;
    public ReactiveField(T a) {
        Value = a;
    }
    public UnityAction<T> action = new UnityAction<T>((T v) => { });
    public T value {
        get {
            return Value;
        }
        set {
            Value = value;
            action.Invoke(value);
        }
    }
}
